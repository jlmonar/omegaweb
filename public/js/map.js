/**
 * Created by Jose Luis on 10/07/17.
 */

function initialize() {
    var myLatLng = {lat: -1.800609, lng: -79.522427};
    var mapOptions = {
        center: myLatLng,
        zoom: 18,
        mapTypeId: google.maps.MapTypeId.roadmap,
        //scrollwheel: false,
       // draggable: false,
        panControl: true,
        zoomControl: true,
        mapTypeControl: true,
        scaleControl: true,
        streetViewControl: true,
        overviewMapControl: true,
        rotateControl: true
    };
    var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'Electrónica Omega'
    });
    marker.setMap(map);
}
google.maps.event.addDomListener(window, 'load', initialize);
