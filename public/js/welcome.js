/**
 * Created by Jose Luis on 24/07/17.
 */

/**
 * Created by Jose Luis on 03/07/17.
 */
$(document).ready(function() {
    //$('#itemslider-audio-video').carousel({ interval: 3000 });

    //$('#itemslider-entretenimiento').carousel({ interval: 3000 });

    //$('#itemslider-3').carousel({ interval: 3000 });

    $('.carousel-showmanymoveone .item').each(function(){
        var itemToClone = $(this);

        for (var i=1;i<6;i++) {
            itemToClone = itemToClone.next();

            if (!itemToClone.length) {
                itemToClone = $(this).siblings(':first');
            }

            itemToClone.children(':first-child').clone()
                .addClass("cloneditem-"+(i))
                .appendTo($(this));
        }
    });
});
