<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoriaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //unique:categoria -> Verifica en la columna nombre de la tabla
            //categorias que no haya otra con el nombre repetido,
            //debe ser unico.
            'nombre' => 'max:256|required|unique:categoria'
        ];
    }
}
