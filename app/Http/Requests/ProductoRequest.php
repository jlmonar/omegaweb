<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'max:50|required|unique:producto',
            'imagen' => 'dimensions:ratio=1|dimensions:min_width=500|image|required' //Para forzar que solo seas archivos de tipo imagen y que sean soo imagenes cuadradas
        ];
    }
}
