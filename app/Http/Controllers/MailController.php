<?php

namespace App\Http\Controllers;

use App\Mail\SendEmail;
use Illuminate\Http\Request;
//use Illuminate\Mail\Mailable;
//use Illuminate\Support\Facades\Mail;
use Mail;


class MailController extends Controller
{
    //
    public function sendEmail() {
        /*
        Mail::send(['text' => 'mail'], ['name', 'José'], function($messqage){
            $messqage->to('jlmonar17@gmail.com', 'José Luis')
                ->subject('prueba.win8.smartest@gmail.com', 'Corporacion Smartest');
            //->subject('jlmonar17@gmail.com', 'Corporacion Smartest');
        });
        */
        try {
            Mail::send(new SendEmail());
            session()->flash('flash_message', '¡Su mensaje ha sido enviado con éxito!');
            session()->flash('alert_class', 'alert-success'); //Tambien hay la clase alert-warning
        } catch(\Exception $ex) {

            session()->flash('flash_message', 'Error al enviar correo -> ' . $ex->getMessage());
            session()->flash('alert_class', 'alert-danger');

        } finally {
            return redirect()->route('contactanos');
        }

    }
}
