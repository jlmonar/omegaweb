<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoriaRequest;
use App\Models\Categoria;
use App\Models\Producto;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class CategoriaController extends Controller
{
    /*
    public function __construct()
    {
        $this->middleware('auth');
    }
    */

    public function  index(Request $request) {
        $categorias = Categoria::search($request->nombre)->orderBy('id', 'DESC')->paginate(5);;
        //$categorias = Categoria::orderBy('id', 'DESC')->paginate(5);
//dd($categorias);
        return view('categorias.index', compact('categorias'));
        //return view('categorias.index')->with('categorias', $categorias);
    }

    public function show() {
        return view('errors.405');
    }

    public function create(Request $request)
    {
        return view('categorias.create');
    }

    /**
     * Método que se encarga de guardar un nuevo regitro de Categoría
     */
    public function store(CategoriaRequest $request) {
        try {
            $categoria = new Categoria();
            $categoria->nombre = $request->nombre;
            $categoria->save();

            //Flash::success('¡La categoria ' . $categoria->nombre . ' ha sido creada con éxito!' );
            session()->flash('flash_message', '¡La categoria ' . $categoria->nombre . ' ha sido creada con éxito!');
            session()->flash('alert_class', 'alert-success'); //Tambien hay la clase alert-warning
        } catch (\Exception $ex) {
            session()->flash('flash_message', 'Error desconocido -> ' . $ex->getMessage());
            session()->flash('alert_class', 'alert-danger');
        } finally {
            return redirect()->route('categorias.index');
        }
    }

    public function edit($id) {
        $categoria = Categoria::find($id);
        if ($categoria == null) {
            return view('errors.405');
        }

        return view('categorias.edit')->with('categoria', $categoria);
    }

    public function update(CategoriaRequest $request, $id) {
        try {
            $categoria = Categoria::find($id);

            $categoria->nombre = $request->nombre;

            $categoria->save();

            session()->flash('flash_message', '¡La categoria ' . $categoria->nombre . ' ha sido actualizada con éxito!');
            session()->flash('alert_class', 'alert-success'); //Tambien hay la clase alert-warning
        } catch (\Exception $ex) {
            session()->flash('flash_message', 'Error desconocido -> ' . $ex->getMessage());
            session()->flash('alert_class', 'alert-danger');
        } finally {
            return redirect()->route('categorias.index');
        }
    }

    public function destroy($id) {
        try {
            $categoria = Categoria::find($id);
            $productos = $categoria->productos();

            //dd(count($productos->get()));
            if (count($productos->get()) > 0 ) {
                session()->flash('flash_message', 'La categoria ' . $categoria->nombre . ' tiene productos asociados, no es posible eliminarla.');
                session()->flash('alert_class', 'alert-warning');
            }
            else {
                $categoria->delete();

                session()->flash('flash_message', '¡La categoria ' . $categoria->nombre . ' ha sido eliminada con éxito!');
                session()->flash('alert_class', 'alert-success'); //Tambien hay la clase alert-warning
            }
        } catch (\Exception $ex) {
            session()->flash('flash_message', 'Error desconocido -> ' . $ex->getMessage());
            session()->flash('alert_class', 'alert-danger');
        } finally {
            return redirect()->route('categorias.index');
        }
    }

    public function productosCategoria(Request $request, $id_categoria) {
        try {
            $categoria = Categoria::find($id_categoria);
            if ($categoria == null) {
                return view('errors.405');
            }
            $productos = $categoria->productos()->paginate(12);

            return view('productos.catalogoProductos', compact('productos', 'categoria'));
        } catch (\Exception $ex) {
            return view('errors.503');
        }
    }
}
