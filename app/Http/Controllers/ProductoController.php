<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductoEditadoRequest;
use App\Http\Requests\ProductoRequest;
use App\Models\Categoria;
use App\Models\Producto;
use Illuminate\Http\Request;

class ProductoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*
    public function __construct()
    {
        $this->middleware('auth');
    }
    */

    public function index(Request $request) {
        //$productos = Producto::orderBy('nombre', 'ASC')->paginate(5);
        $productos = Producto::search($request->nombre)->orderBy('id', 'DESC')->paginate(5);
        return view('productos.index', compact('productos', $productos));
    }

    public function show() {
        return view('errors.405');
    }

    public function create() {
        $categorias = Categoria::orderBy('nombre', 'ASC')->pluck('nombre', 'id');
        return view('productos.create')->with('categorias', $categorias);
    }

    public function store(ProductoRequest $request) {
        try {
            $producto = new Producto();
            $producto->nombre = $request->nombre;
            $producto->marca = $request->marca;
            $producto->disponibilidad = true;
            $producto->precio = $request->precio;
            $producto->descripcion = $request->descripcion;
            $producto->stock = $request->stock;

            $producto->save();

/*

            if ($request->file('imagen')) {
                $file = $request->file('imagen');
                //$name = $producto->id . '.' . $file->getClientOriginalExtension();
                $name = $producto->id . '.jpg';
                $path = public_path() . '/images/productos/';
                $file->move($path, $name);
            }
*/

            //Estos también los guarda en bucket de Amazon, pero no quedan como imagenes publicas
            //$image->store('imagenes', 's3');
            //$image->storeAs('products/'.$producto->id, 'imagen.jpg','s3');

            $s3 = \Storage::disk('s3');
            $image = $request->file('imagen');
            $filePath = '/products/' . $producto->id . '/image.jpg';
            $s3->put($filePath, file_get_contents($image), 'public');

            $producto->categorias()->sync($request->categorias);

            session()->flash('flash_message', '¡El producto ' . $producto->nombre . ' ha sido creado con éxito!');
            session()->flash('alert_class', 'alert-success'); //Tambien hay la clase alert-warning
        } catch(\Exception $ex) {

            session()->flash('flash_message', 'Error desconocido -> ' . $ex->getMessage());
            session()->flash('alert_class', 'alert-danger');

        } finally {
            return redirect()->route('productos.index');
        }
    }

    public function edit($id) {
        $producto = Producto::find($id);
        $categorias = Categoria::orderBy('nombre', 'ASC')->pluck('nombre', 'id');

        if ($producto == null) {
            return view('errors.405');
        }

        $productosCategoria = $producto->categorias->pluck('id')->ToArray();
        //return view('productos.edit', compact('producto', 'categorias', 'productosCategoria'));
        return view('productos.edit')
                    ->with('producto', $producto)
                    ->with('categorias', $categorias)
                    ->with('productosCategoria', $productosCategoria);
    }

    public function update(ProductoEditadoRequest $request, $id) {
        try {
            $producto = Producto::find($id);
            $producto->nombre = $request->nombre;
            $producto->marca = $request->marca;
            //$producto->disponibilidad = true;
            $producto->precio = $request->precio;
            $producto->descripcion = $request->descripcion;
            $producto->stock = $request->stock;

            $producto->save();

            if ($request->file('imagen')) {
                /*
                $file = $request->file('imagen');
                //$name = $producto->id . '.' . $file->getClientOriginalExtension();
                $name = $producto->id . '.jpg';
                $path = public_path() . '/images/productos/';
                $file->move($path, $name);
                */
                $s3 = \Storage::disk('s3');
                $image = $request->file('imagen');
                $filePath = '/products/' . $producto->id . '/image.jpg';
                $s3->put($filePath, file_get_contents($image), 'public');
            }

            $producto->categorias()->sync($request->categorias);

            session()->flash('flash_message', '¡El producto ' . $producto->nombre . ' ha sido editado con éxito!');
            session()->flash('alert_class', 'alert-success'); //Tambien hay la clase alert-warning
        } catch(\Exception $ex) {
            session()->flash('flash_message', 'Error desconocido -> ' . $ex->getMessage());
            session()->flash('alert_class', 'alert-danger');
        } finally {
            return redirect()->route('productos.index');
        }
    }

    public function destroy($id) {
        try {
            $producto = Producto::find($id);
            $producto->delete();

            session()->flash('flash_message', '¡El producto ' . $producto->nombre . ' ha sido eliminado con éxito!');
            session()->flash('alert_class', 'alert-success'); //Tambien hay la clase alert-warning
        } catch (\Exception $ex) {
            session()->flash('flash_message', 'Error desconocido -> ' . $ex->getMessage());
            session()->flash('alert_class', 'alert-danger');
        } finally {
            return redirect()->route('productos.index');
        }
    }

    public function infoProducto(Request $request, $id_producto, $id_categoria = null) {
        $producto = Producto::find($id_producto);
        if ($producto == null) {
            return view('errors.405');
        }

        $categoria = Categoria::find($id_categoria);
        $categorias = $producto->categorias()->get();

        $randomProductos = Producto::productosByRandomCategory($categorias->pluck('id')->toArray(), $producto->id)->inRandomOrder()->get()->take(4);

        return view('productos.infoProducto', compact('producto', 'categoria', 'categorias', 'randomProductos'));
    }

    public function catalogoProductos(Request $request) {
        //$productos = \App\Models\Producto::paginate(3);
        $productos = \App\Models\Producto::paginate(5);
        return view('productos.catalogoProductos', compact('productos'));
    }

    public function productosPorNombre(Request $request) {
        //dd($nombre);
        //dd($request->nombre);
        $busqueda = $request->nombre;
        $productos = Producto::search($request->nombre)->orderBy('id', 'DESC')->paginate(12);
        //$categoria = Categoria::find(1);
        //dd($productos);
        return view('productos.catalogoProductos', compact('productos', 'busqueda'));
        //return view('productos.index', compact('productos', $productos));
        //return redirect()->route('catalogo-productos', $productos);
    }
}
