<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categoria extends Model
{
    use SoftDeletes;

    protected $table = 'categoria';
    protected $primaryKey  = 'id';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Obtiene los productos que pertenecen a la categorias
     */
    public function productos()
    {
        return $this->belongsToMany('App\Models\Producto', 'producto_categoria', 'id_categoria', 'id_producto');
    }

    public function scopeSearch($query, $nombre) {
        return $query->where("nombre", "LIKE", "%$nombre%");

    }
}
