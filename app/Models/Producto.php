<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Producto extends Model
{
    use SoftDeletes;

    protected $table = 'producto';
    protected $primaryKey  = 'id';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Obtiene las categorias del producto
     */
    public function categorias()
    {
        return $this->belongsToMany('App\Models\Categoria', 'producto_categoria', 'id_producto', 'id_categoria');
    }

    public function scopeSearch($query, $text) {
        return $query->where("nombre", "LIKE", "%$text%");
    }

    public function scopeProductosByRandomCategory($query, $categorias, $id_producto) {
        return $query->join('producto_categoria' , 'producto_categoria.id_producto', '=', 'producto.id')
                     ->where('producto.id' , '<>' , $id_producto)
                     ->whereIn('producto_categoria.id_categoria', $categorias)
                     ->distinct('producto_categoria.id_producto')
                     ->select('producto.*');
/*
        $query->join('carrera_proyecto', 'carrera_proyecto.id_proyecto', '=', 'proyecto.id')
            ->where('id_tipo_proyecto', 1)
            ->where('carrera_proyecto.id_carrera', $id_carrera)
            ->where(function ($query) {
                $query->where('id_estado_proyecto', 2)
                    ->orWhere('id_estado_proyecto', 4)
                    ->orWhere('id_estado_proyecto', 5);
            })
            ->select('proyecto.*');// Esto me asegura que retorne solamente las columnas de la tabla Proyecto
*/
    }
}
