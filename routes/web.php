<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $categorias = \App\Models\Categoria::all();
    //dd($categorias->get(0)->productos()->get()[0]->nombre);
    //dd($categorias);
    $array_categorias = array();
    foreach ($categorias as $categoria) {
        $o = new stdClass;
        $o->idCategoria=$categoria->id;
        $o->nombreCategoria=$categoria->nombre;
        $o->productos=$categoria->productos()->orderBy('id','DESC')->take(7)->get();
        array_push($array_categorias, $o);
        //array_push($array_categorias, ($categoria->id : $categoria->nombre));
    }

    //dd($array_categorias);

    /*
    $categorias = $categorias->get(1);

    $productos = $categorias->productos()->get();
    dd($productos);
    */
    return view('welcome', compact('array_categorias'));
})->name('home');

Route::get('/catalogoProductos', function () {
    //$productos = \App\Models\Producto::paginate(3);
    $productos = \App\Models\Producto::paginate(4);
    //$all = \App\Models\Producto::all();
    //dd($all->paginate(2));
    return view('productos.catalogoProductos', compact('productos'));
})->name('catalogo-productos');

Route::get('/quienesSomos', function () {
    return view('quienesSomos');
})->name('quienes-somos');

Route::get('/contactanos', function () {
    return view('contactanos');
})->name('contactanos');

Route::get('/infoProducto/{id_producto}/{id_categoria?}',  ['uses' => 'ProductoController@infoProducto'])->name('info-producto');


Route::get('/productosCategoria/{id_categoria}',  ['uses' => 'CategoriaController@productosCategoria'])->name('catalogo-productos-categoria');

//Route::get('productosPorNombre/{nombre}', ['uses' => 'ProductoController@productosPorNombre'])->name('productos-por-nombre');
Route::get('productosPorNombre', ['uses' => 'ProductoController@productosPorNombre'])->name('productos-por-nombre');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

///////////



///////// MIDDLEWARES
Route::middleware(['auth'])->group(function() {
    Route::resource('productos', 'ProductoController');

    Route::get('productos/{id}/edit', [
        'uses' => 'ProductoController@edit'
    ])->name('editProducto');

    Route::get('productos/{id}/destroy', [
        'uses' => 'ProductoController@destroy'
    ])->name('destroyProducto');


    Route::resource('categorias', 'CategoriaController');

    Route::get('categorias/{id}/destroy', [
        'uses' => 'CategoriaController@destroy'
    ])->name('destroyCategory');

    Route::get('categorias/{id}/edit', [
        'uses' => 'CategoriaController@edit'
    ])->name('editCategory');
});

//Email
Route::post('sendEmail', 'MailController@sendEmail')->name('sendEmail');