@extends('layouts.master')

@section('title','Inicio')

@section('content')
    <div class="welcome-content">
        <row class="slider-menu-section">
            <!--
            <div class="col-xs-12 col-sm-3">
                <div class="menu_wrap">
                    <h3>CATEGORIAS TOP</h3>
                    <ul>
                        <li>
                            <a href="#">Top 20 Electronics Deals!</a>
                        </li>
                        <li>
                            <a href="#">Today's Special Presales</a>
                        </li>
                        <li>
                            <a href="#">5 Or More FlexPay!</a>
                        </li>
                        <li>
                            <a href="#">Free S&amp;H on All Electronics!</a>
                        </li>
                        <li>
                            <a href="#">Electronics on Sale</a>
                        </li>
                        <li>
                            <a href="#">New Arrivals!</a>
                        </li>
                    </ul>
                </div>
            </div>
            -->
            <div class="col-xs-12 col-sm-12">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active item-carousel">
                            <img src="images/sld1.jpg" alt="Los Angeles">
                            <div class="carousel-caption">
                                <h4>Second Thumbnail label</h4>

                                <p>Claim it now.</p>
                            </div>
                        </div>

                        <div class="item item-carousel">
                            <img src="images/sld2.jpg" alt="Chicago">
                            <div class="carousel-caption">
                                <h4>Second Thumbnail label22</h4>

                                <p>Claim it now.</p>
                            </div>
                        </div>

                        <div class="item item-carousel">
                            <img src="images/sld3.jpg" alt="New York">
                            <div class="carousel-caption">
                                <h4>Second Thumbnail label33</h4>

                                <p>Claim it now.</p>
                            </div>
                        </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </row>


        @foreach($array_categorias as $key)
            <row>
                <div class="col-xs-12 col-sm-12 category-list-section">
                    <a class="category-title" title="{{$key->nombreCategoria}}" href="{{route("catalogo-productos-categoria", $key->idCategoria)}}">
                        <h1>{{$key->nombreCategoria}}</h1>
                    </a>
                </div>

                <!-- Item slider-->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="carousel carousel-showmanymoveone slide itemslider" id="itemslider-{{$key->idCategoria}}">
                                <div class="carousel-inner">
                                    @foreach($key->productos as $k=>$producto)
                                        @if($k==0)
                                            <div class="item active">
                                        @else
                                            <div class="item">
                                        @endif
                                        <div class="col-xs-12 col-sm-6 col-md-2 item-col">
                                            <a href="{{route("info-producto", $producto->id)}}">
                                                <div class="item-category">
                                                    <img src="{{env('URL_PRODUCT_IMAGES')}}{{$producto->id}}/image.jpg" class="img-responsive center-block">
                                                    <h4 class="text-center">{{$producto->nombre}}</h4>
                                                    <h5 class="text-center">$ {{number_format($producto->precio, 2)}}</h5>
                                                </div>
                                            </a>
                                        </div>
                                        </div>
                                    @endforeach

                                </div>

                                <div class="slider-control">
                                    <a class="left carousel-control" href="#itemslider-{{$key->idCategoria}}" data-slide="prev">
                                        {{--
                                        <button type="button" class="p_btn100--1st">
                                            <i> < </i>
                                        </button>
                                        --}}
                                        <img src="https://s12.postimg.org/uj3ffq90d/arrow_left.png" alt="Left" class="img-responsive">
                                    </a>
                                    <a class="right carousel-control" href="#itemslider-{{$key->idCategoria}}" data-slide="next"><img src="https://s12.postimg.org/djuh0gxst/arrow_right.png" alt="Right" class="img-responsive"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Item slider end-->
            </row>
        @endforeach
    </div>
@endsection
