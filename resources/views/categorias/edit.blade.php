@extends('layouts.app')

@section('title', 'Edición categoría')

@section('content')
    <div class="container">
        <div class="row">
            <ol class="breadcrumb ">
                <li><a href="{{url('/categorias')}}">Lista de categorias</a></li>
                <li class="active">Edición Categoría - {{$categoria->nombre}}</li>
            </ol>
        </div>

        <div class="row">
            {!! Form::open(['route' => ['categorias.update', $categoria], 'method' => 'PUT' ]) !!}
            <div class="form-group">
                {!!Form::label('nombre','Nombre') !!}
                {!!Form::text('nombre', $categoria->nombre, ['class' => 'form-control', 'placeholder' => 'Nombre de la categoría', 'required']) !!}
            </div>
            <div class="form-group">
                {!! Form::submit('Editar', ['class' => 'btn btn-primary']) !!}
            </div>
            {!!Form::close()!!}
        </div>
    </div>
@endsection