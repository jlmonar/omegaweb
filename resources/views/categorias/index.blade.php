@extends('layouts.app')

@section('title', 'Lista de categorias')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-12" >
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Lista de Categorias
                    </div>
                    <div class="panel-body">
                        <a href=" {{route('categorias.create')}}" class="btn btn-info"> Crear nueva categoría</a>
                        {{-- Buscador de categorias --}}
                        {!! Form::open(['route' => 'categorias.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
                            <div class="form-group">
                                {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Buscar categoría', 'aria-descridbedby' => 'search']) !!}
                            </div>
                                {{--
                                <span id="search-category" class="input-group-addon">
                                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                                </span>
                                --}}
                            <button type="submit" class="btn btn-default">Buscar</button>
                                {{--
                                <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                                --}}

                        {!!Form::close() !!}
                        <hr>
                        <table class="table table-bordered">
                            <thead>
                                <th>Id</th>
                                <th>Nombre</th>
                                <th>Accion</th>
                            </thead>
                            @if(isset($categorias))
                                <tbody>
                                    @foreach($categorias as $categoria)
                                        <tr>
                                            <td>{{$categoria->id}}</td>
                                            <td>{{$categoria->nombre}}</td>
                                            <td >
                                                {{--
                                                <a class="btn btn-warning" href="{{route('editCategory', $categoria->id)}}"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>
                                                <a class="btn btn-danger" href="{{route('destroyCategory', $categoria->id)}}" onclick="return confirm('¿Seguro que desea eliminarlo?')"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a>
                                                --}}
                                                <a class="btn btn-warning" href="{{route('editCategory', $categoria->id)}}">Editar</a>
                                                <a class="btn btn-danger" href="{{route('destroyCategory', $categoria->id)}}" onclick="return confirm('¿Seguro que desea eliminarlo?')">Eliminar</a>
                                                {{--
                                                <button type="button"  >
                                                    <i class="fa fa-pencil fa-lg" aria-hidden="true" style="color:rgb(45, 73, 131);"></i>
                                                </button>
                                                <button type="button" class="sv-icon-button" >
                                                    <i class="fa fa-minus-circle fa-lg" aria-hidden="true" style="color:rgb(255, 0, 0);"></i>
                                                </button>
                                                --}}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            @endif
                        </table>
                        <div class="text-center">
                            {{ $categorias->render() }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection