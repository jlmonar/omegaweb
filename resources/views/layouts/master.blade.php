<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Electrónica Omega::@yield('title')</title>

    <!-- Bootstrap -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    {!!Html::script('js/jquery-2.2.4.min.js')!!}
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    {!!Html::style('css/bootstrap.min.css')!!}
    {!!Html::style('css/global-var.css')!!}
    {!!Html::style('css/master.css')!!}
    {!!Html::style('css/welcome.css')!!}
    {!!Html::style('css/products.css')!!}
    {!!Html::style('css/quienes-somos.css')!!}
    {!!Html::style('css/contacto.css')!!}


    {!!Html::script('js/bootstrap.js')!!}
    <!-- Font awesome icons -->
    {!!Html::style('font-awesome/css/font-awesome.min.css')!!}
    <!-- Mensajes de alerta -->
    {!!Html::script('js/bootbox.min.js')!!}
    <!-- Eventos con delay -->
    {{--
    {!!Html::script('js/bindWithDelay.js')!!}
    --}}
    {!!Html::script('js/welcome.js')!!}
    {!!Html::script('js/products.js')!!}

    <script>
        function myFunction()
        {
            var $searchText = document.getElementById('search-text').value;
            console.log($searchText);
            window.location='productosPorNombre/' + $searchText;
            //window.location='{{route("productos-por-nombre", '$searchText')}}';
        }
    </script>
</head>
<body>

    <div>
        <h2 id="masterTitle">Electrónica Omega</h2>
    </div>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-default sv-nav-bar" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-principal" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                {{--
                <a class="navbar-brand active" href="#"><i class="fa fa-home"></i></a>
                --}}
            </div>

            {{-- NAVBAR HORIZONTALL --}}
            <div id="navbar-principal" class="navbar-header collapse navbar-collapse">
                <ul class="nav navbar-nav" id="navbar-level2">
                    <!--
                    <li class='active'><a id='a-home' href="#" class="fa fa-home">Home</a></li>
                    -->
                    <li {{{ (Route::currentRouteNamed('home') ? 'class=active' : '') }}}><a href="{{route('home') }}">Inicio</a></li>
                    {{--
                    <li {{{ ((Route::currentRouteNamed('catalogo-productos') || Route::currentRouteNamed('info-producto')) ? 'class=active' : '') }}}><a href="{{ route('catalogo-productos') }}">Productos</a></li>
                    --}}
                    <li {{{ ((Route::currentRouteNamed('catalogo-productos-categoria') || Route::currentRouteNamed('info-producto')) ? 'class=active  dropdown' : 'dropdown') }}}>
                        <a href="{{ route('catalogo-productos') }}" class="dropdown-toggle" data-toggle="dropdown">Productos <b class="caret"></b></a>

                        <div class="dropdown-menu row col-lg-12 three-column-navbar" role="menu">

                            <?php $val = round((\App\Models\Categoria::count())/3) ?>
                            {{--
                            @foreach(\App\Models\Categoria::all() as $key=>$categoria)
                            --}}
                            @foreach(\App\Models\Categoria::all() as $key=>$categoria)
                                @if(($key%$val) == 0)
                                    <div class="col-md-4">
                                        <ul>
                                @endif
                                            <li>
                                                <a href="{{ route('catalogo-productos-categoria', $categoria->id) }}">{{$categoria->nombre}}</a>
                                            </li>
                                @if((($key+1)%$val) == 0)
                                        </ul>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </li><!-- /.dropdown -->
                    <li {{{ (Route::currentRouteNamed('quienes-somos') ? 'class=active' : '') }}}><a href="{{route('quienes-somos')}}">Quienes Somos</a></li>
                    <li {{{ ((Route::currentRouteNamed('contactanos')) ? 'class=active' : '') }}}><a href="{{route('contactanos')}}">Ubicación y Contacto</a></li>
                </ul>

                <div class="navbar-form navbar-right" role="search">
                    {{--
                    <div class="form-group" >
                        <div class="input-group">
                            <input name="search-product" placeholder="Buscar." class="form-control"  type="text">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                        </div>
                    </div>
                    --}}

                    {!! Form::open(['route' => 'productos-por-nombre', 'method' => 'GET']) !!}
                        <div class="form-group">
                            {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Buscar producto...', 'aria-descridbedby' => 'search']) !!}
                        </div>
                        <button type="submit" class="btn btn-default">
                            <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                        </button>
                    {!! Form::close() !!}

                    {{--
                    <div class="form-group">
                        {!! Form::text('nombre', null, ['id' => 'search-text','class' => 'form-control', 'placeholder' => 'Buscar producto...', 'aria-descridbedby' => 'search']) !!}
                    </div>
                    <button class="btn btn-default" onclick="myFunction()" >
                        <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                    </button>
                    --}}
                </div>
            </div><!--/.nav-collapse -->

        </div>
    </nav>

    <!-- /container -->
    <div class="content">
        @include('template.partials.flash-message')
        @include('template.partials.errors')
        @yield('content')
    </div>

</body>
<footer class="footer">
    <div>
        Copyright © OMEGA - 2017 All Rights Reserved
        <br>
        Omega es una tienda donde usted podra encontrar toda clase de articulos electrónicos, siempre a su disposición.
        <br>
        Términos y Condiciones
    </div>
</footer>
</html>
