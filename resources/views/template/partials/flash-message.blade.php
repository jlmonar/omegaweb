@if(Session::has('flash_message'))
    <div class="alert {{ Session::get('alert_class') }} alert-dismissible" role="alert" style="text-align:center;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message') }}
    </div>
@endif
