@extends('layouts.master')

@section('title','Quienes Somos')

@section('content')
    {{--
    <h2>QUIENES SOMOS</h2>
    --}}
    <div class="container well container-quienes-somos">
        <row class="row-quienes-somos">
            <p>
                Por más de 128 años hemos acompañado a los colombianos, ofreciéndoles un completo portafolio de productos de calidad y tradición.
            </p>
            <p>
                Nuestras marcas Aguila, Aguila Light, Aguila Cero, Budweiser, Corona, Club Colombia, Costeña, Costeñita, Pilsen, Poker, Redd's, Stella Artois, Cola & Pola y Pony Malta son líderes en Colombia en sus respectivas categorías de bebidas.
            </p>
            <p>
                Nuestros productos son elaborados bajo estándares internacionales de calidad en nuestras seis plantas cerveceras distribuidas geográficamente en Barranquilla, Bucaramanga, Duitama, Medellín, Tocancipá y Yumbo, siendo esta última, la Cervecería del Valle, la más moderna de América Latina. Contamos también con dos malterías en Cartagena y Tibitó (Cundinamarca), dos fábrica de etiquetas y una de tapas.
            </p>
            <p>
                Aportamos significativamente a la economía de nuestro país mediante nuestra actividad industrial, el empleo que generamos, nuestra red de proveedores y distribuidores y los impuestos nacionales y departamentales.
            </p>
        </row>

    </div>
@endsection
