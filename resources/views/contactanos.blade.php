@extends('layouts.master')

@section('title','Contactanos')

@section('content')

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQG4_NxOn9wNcJsL9wOsxbLyhasH2E8DM"></script>
    {!!Html::script('js/map.js')!!}

    <div id='infoProducto' class="container contacto-container ">
        <div class="row contact-row">
            <div class="col-xs-12 col-sm-12">
                <p class="contact-title">UBICACIÓN</p>
            </div>
            <div class="col-xs-12 col-sm-12">
                <div id="map-canvas"></div>
            </div>

            <div class="col-xs-12 col-sm-12">
                <p class="contact-title">CONTÁCTANOS</p>
            </div>
            <div class="col-xs-12 col-sm-7 column-contact">
                {!! Form::open(['route' => 'sendEmail', 'method' => 'POST']) !!}
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input name="name" placeholder="Nombre" class="form-control"  type="text">
                    </div>
                </div>
                <div class="form-group" >
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                        <input name="email" placeholder="Correo electrónico" class="form-control"  type="text">
                    </div>
                </div>

                <div class="form-group" >
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
                        <input name="phone" placeholder="Teléfono" class="form-control" type="text">
                    </div>
                </div>

                <div class="form-group" >
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                        <input name="city" placeholder="Ciudad" class="form-control"  type="text">
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
                        <textarea class="form-control" name="message" placeholder="Escribe tu mensaje..."></textarea>
                    </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                    <button type="submit" class="btn btn-warning" >Enviar <span class="glyphicon glyphicon-send"></span></button>
                </div>
                {!!Form::close()!!}
            </div>
            <div class="col-xs-12 col-sm-5">
                <div id="contact-information" >
                    <h2>CONTACTO</h2>
                    <div class="paragraph bold">¿Tienes alguna duda?</div>
                    <div class="paragraph bold">Envianos un correo electrónico</div>
                    <br>
                    <label>Email:</label>
                    <span>support@omega.com</span>

                    <br>
                    <label>Teléfonos:</label>
                    <span>0985265565 / 052732012</span>

                    <br>
                    <label>Dirección:</label>
                    <span>Babahoyo, Los Ríos, Avenida Universitaria.</span>
                </div>
            </div>
        </div>
    </div>
@endsection
