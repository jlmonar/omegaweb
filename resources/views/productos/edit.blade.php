@extends('layouts.app')

@section('title', 'Edición producto')

@section('content')
    <div class="container">
        <div class="row">
            <ol class="breadcrumb ">
                <li><a href="{{url('/productos')}}">Lista de productos</a></li>
                <li class="active">Edicion Producto - {{$producto->nombre}}</li>
            </ol>
        </div>

        <div class="row">
            {!! Form::open(['route' => ['productos.update', $producto], 'method' => 'PUT', 'files' => true ]) !!}
            <div class="form-group">
                {!!Form::label('nombre','Nombre') !!}
                {!!Form::text('nombre', $producto->nombre, ['class' => 'form-control', 'placeholder' => 'Nombre del producto', 'required']) !!}
            </div>

            <div class="form-group">
                {!!Form::label('marca','Marca') !!}
                {!!Form::text('marca', $producto->marca, ['class' => 'form-control', 'placeholder' => 'Marca del producto', 'required']) !!}
            </div>

            <div class="form-group">
                {!!Form::label('precio','Precio') !!}
                {!!Form::text('precio', $producto->precio, ['class' => 'form-control', 'min' => '0', 'placeholder' => 'Precio del producto', 'required']) !!}
                {{--
                {!!Form::number('precio', null, ['class' => 'form-control', 'min' => '0',  'lang' => 'en-150', 'step'=>'0.01', 'placeholder' => 'Precio del producto', 'required']) !!}
                --}}
            </div>

            <div class="form-group">
                {!!Form::label('stock','Stock') !!}
                {!!Form::number('stock', $producto->stock, ['class' => 'form-control', 'min' => '0', 'placeholder' => 'Stock del producto', 'required']) !!}
            </div>

            <div class="form-group">
                {!!Form::label('imagen','Imagen (El producto ya tiene imagen, seleccione  otra imagen si desea cambiar la actual)') !!}
                {!!Form::file('imagen', ['accept' => 'image/*', 'onchange' => 'readURL(this);']) !!}
            </div>


            <img id="image-selected" src="{{env('URL_PRODUCT_IMAGES')}}{{$producto->id}}/image.jpg" alt="" width="200" height="200"/>
            {{--
            <img id="image-selected" src="../../images/productos/{{$producto->id}}.jpg" alt="" width="200" height="200"/>
            --}}


            <div class="form-group">
                {!!Form::label('descripcion','Descripcion') !!}
                {!!Form::textarea('descripcion', $producto->descripcion, ['class' => 'form-control', 'placeholder' => 'Descripción...', 'required']) !!}
            </div>
            {!!Form::label('categorias','Categorias') !!}
            {!!Form::select('categorias[]', $categorias, $productosCategoria, ['class' => 'form-control select-category', 'multiple', 'required']) !!}
            <div class="form-group">

            </div>

            <div class="form-group">
                {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
            </div>
            {!!Form::close()!!}
        </div>
    </div>
@endsection

@section('js')
    <script>
        //Plugin chosen: https://harvesthq.github.io/chosen/options.html
        $('.select-category').chosen({
            placeholder_text_multiple: 'Seleccione la categorías',
            no_results_text: 'No se encontraron resultados',
            search_contains: true
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#image-selected')
                        .attr('src', e.target.result)
                        .width(200)
                        .height(200);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection

