@extends('layouts.master')

@section('title','Catálogo de productos')

@section('content')
    <div class="container" >
        <div class="row">
            <!-- Title -->
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}">Inicio</a></li>
                @if(isset($categoria))
                    <li class="active">{{$categoria->nombre}}</li>
                @endif

            </ol>
        </div>

        @if(isset($busqueda))
            <h3>Resultado de Búsqueda: #{{$busqueda}}</h3>
            <hr>
        @endif
        @if(isset($productos))
            <div class="row">
                @foreach ($productos as $producto)
                    <div class="col-md-3 col-xs-12 col-sm-4 element-container">
                        <div class="product-container">
                            <div class="product-card">
                                <img src="{{env('URL_PRODUCT_IMAGES')}}{{$producto->id}}/image.jpg" alt="" />
                                <div class="image_overlay"></div>

                                @if(isset($categoria))
                                    <div id="view_details" class="view_details" onclick="window.location='{{ route('info-producto', [$producto->id, $categoria->id]) }}';">VER DETALLES</div>
                                @else
                                    <div id="view_details" class="view_details" onclick="window.location='{{ route('info-producto', $producto->id) }}';">VER DETALLES</div>
                                @endif
                            </div>
                            <div class="stats-container">
                                <div class="product_name">
                                    <span>{{$producto->nombre}}</span>
                                </div>
                                <div class="product_price">
                                    <span >$ {{number_format($producto->precio, 2)}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            {{$productos->links()}}
            {{--
            {{$productos->appends(Request::only('difficulty'))->links()}}
            --}}
        @endif
    </div>
@endsection