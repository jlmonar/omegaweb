@extends('layouts.master')

@section('title','Producto')

@section('content')
    <div id='infoProducto' class="container">
        <div class="row">
            <!-- Title -->
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}">Inicio</a></li>
                @if(isset($categoria))
                    <li><a href="{{ route('catalogo-productos-categoria', $categoria->id) }}">{{$categoria->nombre}}</a></li>
                @else
                @endif
                @if(isset($producto))
                    <li class="active">{{$producto->nombre}}</li>
                @endif
            </ol>
        </div>
        <div class="row">
            <!-- Product Info-->
            @if(isset($producto))
                <div class="col-xs-12 col-sm-6">
                    <img src="{{env('URL_PRODUCT_IMAGES')}}{{$producto->id}}/image.jpg" class="img-responsive img_product">
                </div>
                <div class="col-xs-12 col-sm-6">
                    <h2 id="product-title">{{$producto->nombre}} </h2>
                    <hr>

                    <h5>Disponible</h5>
                    <p class='product_description'>
                        {{$producto->descripcion}}
                        {{--
                        Fully lined interior with zipper accessory pocket, metal feet on bottom to protect bag, accented with polished nickel hardware. Zippers top opening. 20.5 inch width x 12.5 inch height x 5.5 inch depth. ALG188
                        --}}
                    </p>

                    <br>
                    <p class="product_prize">$ {{number_format($producto->precio, 2)}}</p>

                    <br>
                    <label>Marca:</label>
                    <span>{{$producto->marca}}</span>

                    <br><br>
                    <label>Código:</label>
                    <span>PRl0151</span>

                    <br><br>
                    <label>Stock:</label>
                    <span>{{$producto->stock}}</span>

                    <br><br>
                    <label>Categorias:</label>
                    @if(isset($categorias))
                        @foreach($categorias as $categoria)
                            <a href="{{ route('catalogo-productos-categoria', $categoria->id) }}">{{$categoria->nombre}}</a>,
                        @endforeach
                    @endif
                </div>
            @endif


        </div>
        <div class="row" style="margin-top: 40px">
            <h3>Productos relacionados</h3>
            <hr>
        </div>

        @if(isset($randomProductos))
            <div class="row">
                @foreach ($randomProductos as $producto)
                    <div class="col-md-3 col-xs-12 col-sm-4 element-container">
                        <div class="product-container">
                            <div class="product-card">
                                <img src="{{env('URL_PRODUCT_IMAGES')}}{{$producto->id}}/image.jpg" alt="" />
                                <div class="image_overlay"></div>

                                <div id="view_details" class="view_details" onclick="window.location='{{ route('info-producto', $producto->id) }}';">VER DETALLES</div>
                            </div>
                            <div class="stats-container">
                                <div class="product_name">
                                    <span>{{$producto->nombre}}</span>
                                </div>
                                <div class="product_price">
                                    <span >$ {{number_format($producto->precio, 2)}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endif
    </div>
@endsection