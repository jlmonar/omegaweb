@extends('layouts.app')

@section('title', 'Lista de productos')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-12" >
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Lista de Productos
                    </div>
                    <div class="panel-body">
                        <a href=" {{route('productos.create')}}" class="btn btn-info"> Crear nuevo producto</a>
                        {{-- Buscador de productos --}}
                        {!! Form::open(['route' => 'productos.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
                        <div class="form-group">
                            {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Buscar producto', 'aria-descridbedby' => 'search']) !!}
                        </div>
                        {{--
                        <span id="search-category" class="input-group-addon">
                            <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                        </span>
                        --}}
                        <button type="submit" class="btn btn-default">Buscar</button>
                        {{--
                        <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                        --}}

                        {!!Form::close() !!}
                        <hr>
                        <table class="table table-bordered">
                            <thead>
                            <th>Nombre</th>
                            <th>Precio</th>
                            <th>Stock</th>
                            <th>Accion</th>
                            </thead>
                            @if(isset($productos))
                                <tbody>
                                @foreach($productos as $producto)
                                    <tr>
                                        <td>{{$producto->nombre}}</td>
                                        <td>{{$producto->precio}}</td>
                                        <td>{{$producto->stock}}</td>
                                        <td >
                                            {{--
                                            <a class="btn btn-warning" href="{{route('editProducto', $producto->id)}}"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>
                                            <a class="btn btn-danger" href="{{route('destroyProducto', $producto->id)}}" onclick="return confirm('¿Seguro que desea eliminarlo?')"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a>
                                            --}}
                                            <a class="btn btn-warning" href="{{route('editProducto', $producto->id)}}">Editar</a>
                                            <a class="btn btn-danger" href="{{route('destroyProducto', $producto->id)}}" onclick="return confirm('¿Seguro que desea eliminarlo?')">Eliminar</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            @endif
                        </table>
                        <div class="text-center">
                            {{ $productos->render() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection